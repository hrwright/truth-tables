//
// Created by Haley Manning on 10/15/17.
//

#ifndef MATH_HELP_POWER_H
#define MATH_HELP_POWER_H

#include <iostream>

using namespace std;

class Power {
private:
  int n;
  int timesHit;
  int answer;

  int pow(int);

  void print();

public:
  Power(int);

};


#endif //MATH_HELP_POWER_H
