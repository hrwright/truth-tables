//
// Created by Haley Manning on 9/15/17.
//

#include "TruthTable.h"

TruthTable::TruthTable() {
  // empty constructor
  initialize();
  findAnswer();
}

TruthTable::TruthTable(TableType tp) {
  type = tp;
  initialize();
  findAnswer();
}

TruthTable::TruthTable(string a, string b) {
  name1 = a;
  name2 = b;
  initialize();
  findAnswer();
}

TruthTable::TruthTable(vector<bool> v1, vector<bool> v2, TableType tp) {
  a = v1;
  b = v2;
  type = tp;
  initializeAnswer();
  findAnswer();
}

TruthTable::TruthTable(vector<bool> v1, TableType tp) {
  a = v1;
  type = tp;
  initializeAnswer();
  findAnswer();
}

void TruthTable::initializeAnswer() {
  for (int i = 0; i < SIZE; ++i) {
    answer.push_back(0);
  }
}

void TruthTable::initialize() {
  a.push_back(1);
  a.push_back(1);
  a.push_back(0);
  a.push_back(0);
  b.push_back(1);
  b.push_back(0);
  b.push_back(1);
  b.push_back(0);

  initializeAnswer();
}

void TruthTable::resetAnswer() {
  for(int i=0; i<SIZE; i++){
    answer[i] = 0;
  }
}

void TruthTable::printType() {
  switch (type) {
    case AND:
      cout<<" ^ ";
      break;
    case OR:
      cout<<" V ";
      break;
    case IF_THEN:
      cout<<" => ";
      break;
    case IF_AND_ONLY_IF:
      cout<<" <=> ";
      break;
    case NOT:
      cout<<" ! ";
      break;
    case XOR:
      cout<<" XOR ";
      break;
    case XAND:
      cout<<" XAND ";
      break;
    default:
      break;
  }
}

void TruthTable::printTable() {
  if (type == NONE) return;
  cout<<endl;
  if(type != NOT){
    cout<<name1<<" | "<<name2<<" |";
    printType();
    cout<<endl;
    for(int i=0; i<SIZE; i++){
      for(int j=0; j<1; j++){
        if(a[i]) cout<<setw(2)<<"T ";
        else cout<<setw(2)<<"F ";
        cout<<"| ";

        if(b[i]) cout<<setw(2)<<"T ";
        else cout<<setw(2)<<"F ";
        cout<<"| ";

        if(answer[i]) cout<<setw(2)<<"T ";
        else cout<<setw(2)<<"F ";
      }
      cout<<endl;
    }
  }
  else {
    cout<<name1<<" |";
    printType();
    cout<<endl;
    for (int i = 0; i < SIZE; ++i) {
      for (int j = 0; j <1 ; ++j) {
        if(a[i]) cout<<setw(2)<<"T ";
        else cout<<setw(2)<<"F ";
        cout<<"| ";

        if(answer[i]) cout<<setw(2)<<"T ";
        else cout<<setw(2)<<"F ";
      }
      cout<<endl;
    }
  }
}

vector<bool> TruthTable::findAnswer() {
  switch (type) {
    case AND:
      for(int i=0; i<SIZE; i++){
        answer[i] = a[i] && b[i];
      }
      break;
    case OR:
      for(int i=0; i<SIZE; i++){
        answer[i] = a[i] || b[i];
      }
      break;
    case IF_THEN:
      for(int i=0; i<SIZE; i++){
        if(a[i] && !b[i]) answer[i] = 0;
        else answer[i] = 1;
      }
      break;
    case IF_AND_ONLY_IF:
      for(int i=0; i<SIZE; i++){
        if(a[i] && b[i] || !a[i] && !b[i]) answer[i] = 1;
        else answer[i] = 0;
      }
      break;
    case NOT:
      for(int i=0; i<SIZE; i++){
        answer[i] = !a[i];
      }
      break;
    case XOR:
      for(int i=0; i<SIZE; i++){
        answer[i] = a[i] ^ b[i];
      }
      break;
    case XAND:
      for(int i=0; i<SIZE; i++){
        if(a[i] && b[i]) answer[i] = true;
        else if(!a[i] && !b[i]) answer[i] = true;
        else answer[i] = false;
      }
      break;
    default:
      break;
  }
  printTable();
  return getAnswer();
}