//
// Created by Haley Manning on 9/15/17.
//

#ifndef MATH_HELP_TRUTHTABLE_H
#define MATH_HELP_TRUTHTABLE_H

#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

enum TableType {
  AND, OR, IF_THEN, IF_AND_ONLY_IF, NOT, NONE, XOR, XAND
};

class TruthTable {
private:
  const int SIZE = 4;
  vector<bool> a;
  vector<bool> b;
  vector<bool> answer;
  string name1 = "a";
  string name2 = "b";
  TableType type = NONE;

  void resetAnswer();
  void printType();
  void initialize();
  void initializeAnswer();
  vector<bool> findAnswer();
public:
  TruthTable();
  TruthTable(TableType tp);
  TruthTable(string a, string b);
  TruthTable(vector<bool>, vector<bool>, TableType);
  TruthTable(vector<bool>, TableType);

  vector<bool> getAnswer(){ return answer; }
  void printTable();
};


#endif //MATH_HELP_TRUTHTABLE_H
