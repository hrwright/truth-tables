//
// Created by Haley Manning on 10/15/17.
//

#include "Power.h"

Power::Power(int start) {
  n=start;
  timesHit=0;
  answer = pow(n);
  print();
}

int Power::pow(int num) {
  timesHit++;
  if(num < 0){
    cout<<"Knock it off!"<<endl;
    return -1;
  }
  if(num == 0) return 0;
  if(num == 1) return 1;
  if( num > 1){
    return 3*pow(num-1)-2*pow(num-2);}
}

void Power::print() {
  cout<<"----------------------"<<endl;
  cout<<"Number: "<<n<<endl;
  cout<<"Times hit: "<<timesHit<<endl;
  cout<<"Answer: " << answer <<endl<<endl;
}